$(document).ready(function() {

	var ViewModel = function() {
		var self = this;

		self.comics = ko.observableArray();

		self.deleteComic = function(comic) {
			console.info('Deleting Comic with Id "'+comic.id+'"');
			self.comics.remove(comic);
			$.getJSON(BASE_URL+'admin/ajax/delete/'+comic.id, function(d) {
				console.info(d);
			});
		};
	};

	var vm = new ViewModel();
	ko.applyBindings(vm);

	$.getJSON(BASE_URL+'admin/ajax/comics', function(d) {
		vm.comics(d);
	});
});