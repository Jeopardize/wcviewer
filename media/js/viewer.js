$(document).ready(function(){

	var ViewModel = function() {
		var self = this;
		self.currentComic = ko.observable();
		self.isLoading = ko.observable(false);
		self.isLastComic = ko.observable(false);
		self.isFirstComic = ko.observable(false);

		self.loadPrevious = function() {
			if (self.isFirstComic())
				return;
			self.loadComic('previous/'+self.currentComic().id);
		};
		self.loadNext = function() {
			if (self.isLastComic())
				return;
			self.loadComic('next/'+self.currentComic().id);
		};
		self.loadFirst = function() {
			if (self.isFirstComic())
				return;
			self.loadComic('first');
		};
		self.loadLast = function() {
			if (self.isLastComic())
				return;
			self.loadComic('last');
		};

		self.path = function() {
			return BASE+self.currentComic().path;
		};

		self.loadComic = function(rel) {
			if (self.isLoading())
				return;

			self.isLoading(true);
			$.getJSON(BASE_URL+'ajax/'+rel, function(c) {
				vm.currentComic(c['comic']);
				vm.isFirstComic(c['is_first']);
				vm.isLastComic(c['is_last']);
				console.info(c);
				self.isLoading(false);
			});
		}
	};

	var vm = new ViewModel();
	ko.applyBindings(vm);

	vm.loadLast();

});