<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'driver'       => 'File',
	'hash_method'  => 'sha512',

	'hash_key'     => '', // Insert hash key between ''

	'lifetime'     => 1209600,
	'session_type' => Session::$default,
	'session_key'  => 'auth_user',
	'users' => array(

		'admin' => '' // Insert password hash between ''

	),

);
