<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
	<title><?= $title ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<?= Template::generate_styles($styles) ?>

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<header>
</header>
<?php if ($show_header): ?>
	<div id="header">
		<div class="container">
		<div class="sixteen columns">
			<?php if (Auth::instance()->logged_in()): ?>
				<div id="user-info">
					<span>Welcome, </span>
					<span style="font-weight:bold;"><?= Auth::instance()->get_user() ?></span>
					(<a href="<?= URL::site('auth/logout') ?>">Logout</a>)
				</div>
			<?php endif ?>
			<div class="clear"></div>
			<div id="site-name">
				<?= $title ?>
			</div>
			<ul id="navigation">
				<?php foreach($navigation as $k => $v): ?>
					<li><a href="<?= $v ?>"><?= $k ?></a></li>
				<?php endforeach ?>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php endif ?>
<div class="container">
<?= $content ?>
</div>
<div class="clear"></div>
<?php if ($show_footer): ?>
<div id="footer">
	<p>Comic Viewer 0.3a</p>
	<?php if ($show_backend_link): ?>
	<a href="<?= URL::site('/admin') ?>">Backend</a>
	<?php endif ?>
</div>
<?php endif ?>

<script>
	var BASE_URL = '<?= URL::site('/') ?>';
	var BASE = '<?= URL::base() ?>';
</script>
<?= Template::generate_scripts($scripts, 0) ?>
</div>
</body>
</html>