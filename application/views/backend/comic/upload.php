<div class="sixteen columns">
	<h4>Upload Comic</h4>

	<form action="#" method="post" enctype="multipart/form-data">
		<label for="up-title">Title</label>
		<input type="text" id="up-title" name="comic-title" />

		<label for="up-image">Image</label>
		<input type="file" id="up-image" name="comic-image" /><br /><br />

		<input type="submit" class="button" value="Upload" />
	</form>
</div>
