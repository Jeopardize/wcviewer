<div class="sixteen columns">
	<a href="<?= URL::site('admin/comic/upload') ?>" class="button">Upload New</a>
	<table id="comics-table">
		<thead>
		<tr>
			<th>Title</th>
			<th>Upload Date</th>
			<th>&nbsp;</th>
		</tr>
		</thead>
		<tbody data-bind="foreach: comics">
			<tr>
				<td data-bind="text: title"></td>
				<td data-bind="text: uploaded_at"></td>
				<td>
					<a href="#" class="button button-delete" data-bind="click: $parent.deleteComic, attr: {'data-id':id}">
						<img src="<?= URL::base() ?>media/images/icon-delete-32.png" />
						<span>Delete</span>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>
