<div class="inner-container">
<h1>Login</h1>
<?php if($auth_error): ?>
<div>
	<p>Login failed. Either the password or the username was wrong.</p>
</div>
<?php endif ?>
<form action="#" method="post" class="form-login">

	<label for="auth-name">Name</label>
	<input type="text" id="auth-name" name="auth-name" />

	<label for="auth-password">Password</label>
	<input type="password" id="auth-password" name="auth-password" />

	<input type="submit" value="Login" />
	<a href="#" style="padding-left: 10px;">Forgot password</a>

</form>
</div>
