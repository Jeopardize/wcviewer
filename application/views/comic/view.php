<div class="sixteen columns">
	<div id="image-container" data-bind="with: currentComic">
		<h1 id="comic-title" data-bind="text: title"></h1>

		<?= View::factory('comic/navigation')->render() ?>

		<img id="comic-image" data-bind="attr: {src: $parent.path()}" alt=""/>

		<?= View::factory('comic/navigation')->render() ?>
	</div>
</div>