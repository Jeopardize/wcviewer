<div class="comic-nav">
	<a href="#" class="nav-button nav-button-first"
	   data-bind="css: {'disabled': $parent.isLoading() || $parent.isFirstComic()}, click: $parent.loadFirst">
		<div class="nav-inner-image nav-inner-image-first"></div>
	</a>
	<a href="#" class="nav-button"
	   data-bind="css: {'disabled': $parent.isLoading() || $parent.isFirstComic()}, click: $parent.loadPrevious">
		<div class="nav-inner-image nav-inner-image-previous"></div>
	</a>

	<span class="comic-nav-spacer"></span>

	<a href="#" class="nav-button"
	   data-bind="css: {'disabled': $parent.isLoading() || $parent.isLastComic()}, click: $parent.loadNext">
		<div class="nav-inner-image nav-inner-image-next"></div>
	</a>
	<a href="#" class="nav-button"
	   data-bind="css: {'disabled': $parent.isLoading() || $parent.isLastComic()}, click: $parent.loadLast">
		<div class="nav-inner-image nav-inner-image-last"></div>
	</a>
</div>