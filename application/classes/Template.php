<?php defined('SYSPATH') or die('No direct script access.');

class Template {

	/**
	 * Return stylesheets as HTML.
	 * @param $styles
	 * @param int $indent_level
	 * @return bool|string
	 */
	public static function generate_styles($styles, $indent_level = 1)
	{
		if ( ! is_array($styles))
		{
			return FALSE;
		}
		$result = '';
		foreach($styles as $style)
		{
			$result .= HTML::style($style)."\n".str_repeat("\t", $indent_level);
		}
		return $result;
	}

	public static function generate_scripts($scripts, $indent_level = 1)
	{
		if ( ! is_array($scripts))
		{
			return FALSE;
		}
		$result = '';
		foreach ($scripts as $script)
		{
			$result .= HTML::script($script)."\n".str_repeat("\t", $indent_level);
		}
		return $result;
	}

}