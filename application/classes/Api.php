<?php defined('SYSPATH') or die('No direct script access.');

class Api {

	/**
	 * Convert the given array of ORM models to JSON
	 * @param ORM[] $array
	 * @return string
	 */
	public static function json_encode_array($array)
	{
		$result = array();

		foreach ($array as $obj)
		{
			array_push($result, $obj->as_array());
		}

		return json_encode($result);
	}

}
