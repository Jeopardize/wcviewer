<?php

class Task_Auth_GenerateHash extends Minion_Task {

	protected $_options = array(
		'length' => 8,
	);

	protected function _execute(array $params)
	{
		$result = '';
		$charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		for ($i = 0; $i < $params['length']; $i++)
			$result .= substr($charset, rand(0, strlen($charset) - 1), 1);
		echo "Random Hash: ".$result."\n";
	}

}
