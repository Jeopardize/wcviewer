<?php defined('SYSPATH') or die('No direct script access.');

class Task_Auth_HashPassword extends Minion_Task {

	protected $_options = array(
		'password' => NULL
	);

	protected function _execute(array $params)
	{
		if ($params['password'] === NULL)
		{
			echo "ERROR: Missing required password.\n";
			return;
		}

		echo "Copy the following hash to application/config/auth.php\n\n"
			.Auth::instance()->hash($params['password'])."\n\n";
	}

	protected function generate_random_hash($length = 8)
	{

	}

}
