<?php defined('SYSPATH') or die('No direct script access.');

class Task_Database_LoadSamples extends Minion_Task {

	protected $_options = array();

	protected function _execute(array $params)
	{
		echo "Loading sample data...\n";
		$this->_empty_tables();

		$this->_load_comics();
		$this->_load_settings();

		echo "Done.\n";
	}

	protected function _load_comics()
	{
		$comics = array(
			array(
				'title' => 'The First Comic',
				'uploaded_at' => '2013-03-04',
				'file_ext' => 'png',
			),
			array(
				'title' => 'The Second Comic',
				'uploaded_at' => '2013-03-05',
				'file_ext' => 'png',
			),
			array(
				'title' => 'The Third Comic',
				'uploaded_at' => '2013-03-06',
				'file_ext' => 'png',
			),
		);

		foreach ($comics as $comic)
		{
			$c = ORM::factory('Comic');

			$c->title = $comic['title'];
			$c->uploaded_at = $comic['uploaded_at'];
			$c->file_ext = $comic['file_ext'];
			$c->save();

		}
	}

	protected function _load_settings()
	{
		Settings::set('show_header', 'true');
		Settings::set('show_footer', 'true');
		Settings::set('theme', 'light');
	}

	protected function _empty_tables()
	{
		$db = Database::instance();
		$db->query(NULL, 'TRUNCATE TABLE  `comics`');
		$db->query(NULL, 'TRUNCATE TABLE `settings`');
	}

}
