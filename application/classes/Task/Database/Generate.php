<?php defined('SYSPATH') or die('No direct script access.');

class Task_Database_Generate extends Minion_Task {

	protected function _execute(array $params)
	{
		echo "Generating database scheme...\n";

		$sql = file_get_contents(DOCROOT.'scheme.sql');
		Database::instance()->query(NULL, $sql);

		echo "Done.\n";
	}

}
