<?php defined('SYSPATH') or die('No direct script access.');

class Model_Comic extends ORM {

    /**
     * The columns (required for PDO, because list_columns is not supported)
     */
    protected $_table_columns = array(
        'id' => NULL,
        'title' => NULL,
        'uploaded_at' => NULL,
        'file_ext' => NULL,
    );

	/**
	 * Returns path where all comics are stored.
	 * @return string
	 */
	public function get_path()
	{
		return 'media/comics/';
	}

	/**
	 * Returns file name, including file extension.
	 * @return string
	 */
	public function get_filename()
	{
		// Only take 40 characters of the title, to keep the file name short.
		return substr(urlencode($this->title), 0, 40).'.'.$this->file_ext;
	}

	/**
	 * Upload image to the corresponding storage folder.
	 * @param $file
	 * @return  string  on success, full path to new file
	 * @return  bool    on failure (FALSE)
	 * @throws Kohana_Exception
	 */
	public function upload_image($file)
	{
		if ( ! $this->save())
			throw new Kohana_Exception("Comic Entity must be saved before uploading image.");

		return Upload::save($file, $this->get_filename(), $this->get_path());
	}

	/**
	 * Extend as_array() by adding the full path to the image.
	 * @return array
	 */
	public function as_array()
	{
		$array = parent::as_array();
		$array['path'] = $this->get_path().$this->get_filename();
		return $array;
	}

}