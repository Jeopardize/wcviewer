<?php defined('SYSPATH') or die('No direct script access.');

class Settings {

	/**
	 * @param string $name Name of the option.
	 * @param null $default_value Returned if no option with the given name is found.
	 * @return string|null
	 */
	public static function get($name, $default_value = NULL)
	{
		$result = DB::select()
			->from('settings')
			->where('name', '=', $name)
			->execute();

		return isset($result[0])
			? $result[0]['value']
			: $default_value;
	}

	/**
	 * Either creates or updates the setting with the given name.
	 * @param string $name Name of the option.
	 * @param string $value Value that you want to set it to.
	 */
	public static function set($name, $value)
	{
		if (self::get($name) === NULL)
		{
			DB::insert('settings', array('name', 'value'))
				->values(array($name, $value))
				->execute();
		}
		else
		{
			DB::update('settings')
				->set(array('value' => $value))
				->where('name', '=', $name)
				->execute();
		}
	}

}