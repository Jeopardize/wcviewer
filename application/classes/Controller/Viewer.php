<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Viewer extends Controller_Base {

	/**
	 * Show the first comic.
	 */
	public function action_index()
	{
		// TODO: Get the latest one instead of the one with ID 1.
		$this->template->set_global('comic', ORM::factory('Comic')->where('id','=',1)->find());

		array_push($this->template->scripts, 'media/js/viewer.js');
		$this->template->content = View::factory('comic/view');
	}

}