<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Base extends Controller_Template {

	public function before()
	{
		parent::before();

		// Visibilty of html elements
		$this->template->show_header = Settings::get('show_header');
		$this->template->show_footer = Settings::get('show_footer');
		$this->template->show_backend_link = TRUE;

		$this->template->title = Settings::get('site_name');

		$theme = Settings::get('theme');

		$this->template->styles = array(
			'media/css/themes/'.$theme.'.css',
		);

		$this->template->scripts = array(
			'media/js/jquery-2.0.2.min.js',
			'media/js/knockout-2.3.0.min.js',
		);

		$this->template->navigation = array(
			'Latest'  => URL::site('/'),
			'Archive' => URL::site('/'),
		);
	}

	public function after()
	{
		parent::after();
	}

}
