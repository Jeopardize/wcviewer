<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Backend_Base extends Controller_Base {

	public function before()
	{
		parent::before();

		// Every backend controller requires authentication
		// Since the AJAX controller does not extend this one, this piece of code is also included there.
		// TODO: Let AJAX Controller extend this one, prevent rendering of HTML response.
		if ( ! Auth::instance()->logged_in())
		{
			$this->redirect('auth/login');
			return;
		}

		$this->template->show_backend_link = FALSE;

		$this->template->title = 'Comic Viewer';

		// Overwrite default navigation, defined by base controller
		$this->template->navigation = array(
			'Frontend' => URL::site('/'),
			'Manage Comics' => URL::site('admin/comic/list'),
		);

		// Add backend specific file to the stylesheets
		array_push($this->template->styles, 'media/css/backend.css');
	}

	public function after()
	{
		parent::after();
	}

}