<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Backend_Ajax extends Controller {

	public function before()
	{
		if ( ! Auth::instance()->logged_in())
		{
			$this->redirect('auth/login');
		}
	}

	public function action_comics()
	{
		$comics = ORM::factory('Comic')
			->find_all()
			->as_array();

		echo Api::json_encode_array($comics);
	}

	public function action_delete()
	{
		$id = $this->request->param('id');

		if ($id === NULL)
		{
			echo json_encode(array('error' => 'Missing required parameter id.'));
			return;
		}

		$comic = ORM::factory('Comic', $id);

		if ( ! $comic->loaded())
		{
			echo json_encode(array('error' => 'Comic not found.'));
			return;
		}

		$comic->delete();

		echo json_encode(array('success' => TRUE));
	}

}

