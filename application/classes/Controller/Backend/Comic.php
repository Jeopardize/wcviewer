<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Backend_Comic extends Controller_Backend_Base {

	public function action_upload()
	{
		if ($this->request->method() == Request::POST)
		{
			$comic = new Model_Comic();
			$comic->title = $this->request->post('comic-title');
			$comic->uploaded_at = DB::expr('NOW()');
			// TODO: Get file extension and validate.
			$comic->file_ext = 'png';

			// Save to get ID, used for assigning filename.
			$comic->save();
			$comic->upload_image($_FILES['comic-image']);
		}
		$this->template->content = View::factory('backend/comic/upload');
	}

	public function action_list()
	{
		array_push($this->template->scripts, 'media/js/backend_list.js');
		$this->template->content = View::factory('backend/comic/list');
	}

}