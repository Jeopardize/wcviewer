<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {

	public function action_last()
	{
		$comic = ORM::factory('Comic')
			->order_by('uploaded_at', 'desc')
			->find();

		if ($comic->loaded())
		{
			echo $this->_generate_json($comic, FALSE, TRUE);
		}
		else
		{
			$no_comic_entity = ORM::factory('Comic');
			$no_comic_entity->id = 0;
			$no_comic_entity->title = 'No pages';
			$no_comic_entity->file_ext = 'png';
			echo $this->_generate_json($no_comic_entity, TRUE, TRUE);
		}
	}

	public function action_first()
	{
		$comic = ORM::factory('Comic')
			->order_by('uploaded_at', 'asc')
			->find();

		echo $this->_generate_json($comic, TRUE, FALSE);
	}

	public function action_previous()
	{
		$comic_current = ORM::factory('Comic', $this->request->param('id'));
		$comic = ORM::factory('Comic')
			->order_by('uploaded_at', 'desc')
			->where('uploaded_at', '<', $comic_current->uploaded_at)
			->limit(2)
			->find_all();

		if ($comic->count() == 0)
			return;

		echo $this->_generate_json($comic[0], $comic->count() == 1, FALSE);
	}

	public function action_next()
	{
		$comic_current = ORM::factory('Comic', $this->request->param('id'));
		$comic = ORM::factory('Comic')
			->order_by('uploaded_at', 'asc')
			->where('uploaded_at', '>', $comic_current->uploaded_at)
			->limit(2)
			->find_all();

		if ($comic->count() == 0)
			return;

		echo $this->_generate_json($comic[0], FALSE, $comic->count() == 1);
	}

	private function _generate_json($comic, $is_first, $is_last)
	{
		return json_encode(array(
				'is_first' => $is_first,
				'is_last'  => $is_last,
				'comic'    => $comic->as_array(),
		));
	}

}