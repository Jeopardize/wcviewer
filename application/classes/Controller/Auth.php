<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Base {

	public function action_login()
	{
		$this->template->set_global('auth_error', FALSE);

		if ($this->request->method() === Request::POST)
		{
			$name = $this->request->post('auth-name');
			$password = $this->request->post('auth-password');

			$result = Auth::instance()->login($name, $password);
			echo Debug::vars($result, $name, $password);

			if ( ! $result)
			{
				$this->template->set_global('auth_error', TRUE);
			}
			else
			{
				$this->redirect('/admin/comic/list');
			}
		}

		$this->template->content = View::factory('auth/login');
	}

	public function action_logout()
	{
		Auth::instance()->logout();
		$this->redirect('/');
	}

}

